from decimal import Decimal
import datetime

from PyQt5 import QtCore


class ActiveOrder(QtCore.QObject):
    __order_id = 0
    __pair = None
    __currency = None
    __type = ''
    __amount = 0
    __rate = 0
    __created_at = None
    __timestamp_created = 0

    def __init__(self, order_id, pair, currency, type, amount, rate, timestamp_created):
        self.__order_id = order_id
        self.__currency = currency
        self.__pair = pair
        self.__type = type
        self.__amount = amount
        self.__rate = rate
        self.__timestamp_created = timestamp_created
        self.__created_at = datetime.datetime.fromtimestamp(int(timestamp_created)).strftime('%Y-%m-%d %H:%M:%S')

        super().__init__()

    @property
    def order_id(self) -> int:
        return self.__order_id

    @property
    def pair(self) -> str:
        return self.__pair

    @property
    def currency(self) -> str:
        return self.__currency

    @property
    def type(self) -> str:
        return self.__type

    @property
    def amount(self) -> Decimal:
        return self.__amount

    @property
    def rate(self) -> Decimal:
        return self.__rate

    @property
    def created_at(self) -> Decimal:
        return self.__created_at
