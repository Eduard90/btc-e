from decimal import Decimal
import datetime

from PyQt5 import QtCore


class Trade(QtCore.QObject):
    __pair = ''
    __type = ''
    __rate = 0
    __amount = 0
    __price = 0
    __timestamp = 0
    __date = None

    def __init__(self, pair, type, rate, amount, timestamp):
        self.__pair = pair
        self.__type = type
        self.__rate = rate
        self.__amount = amount
        self.__timestamp = timestamp
        self.__date = datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
        self.__price = self.__rate * self.__amount

        super().__init__()

    @property
    def pair(self) -> str:
        return self.__pair

    @property
    def type(self) -> str:
        return self.__type

    @property
    def rate(self) -> Decimal:
        return self.__rate

    @property
    def amount(self) -> Decimal:
        return self.__amount

    @property
    def price(self) -> Decimal:
        return self.__price

    @property
    def date(self) -> str:
        return self.__date
