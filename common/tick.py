from decimal import Decimal

from PyQt5 import QtCore


class Tick(QtCore.QObject):
    __pair = None
    __low = 0
    __high = 0
    __last = 0
    __updated = None
    __currency = None

    def __init__(self, currency, pair, low, high, last, updated):
        self.__currency = currency
        self.__pair = pair
        self.__low = low
        self.__high = high
        self.__last = last
        self.__updated = updated

        super().__init__()

    @property
    def currency(self) -> str:
        return self.__currency

    @property
    def pair(self) -> str:
        return self.__pair

    @property
    def low(self) -> Decimal:
        return self.__low

    @property
    def high(self) -> Decimal:
        return self.__high

    @property
    def last(self) -> Decimal:
        return self.__last

    @property
    def updated(self) -> Decimal:
        return self.__updated
