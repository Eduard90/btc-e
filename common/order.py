from decimal import Decimal

from PyQt5 import QtCore


class Order(QtCore.QObject):
    __rate = 0
    __amount = 0
    __price = 0

    def __init__(self, rate, amount):
        self.__rate = rate
        self.__amount = amount
        self.__price = self.__rate * self.__amount

        super().__init__()

    @property
    def rate(self) -> Decimal:
        return self.__rate

    @property
    def amount(self) -> Decimal:
        return self.__amount

    @property
    def price(self) -> Decimal:
        return self.__price
