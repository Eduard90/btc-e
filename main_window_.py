import os
from PyQt5 import QtCore, QtGui, QtWidgets

from bot.thread import BotThread
import settings
from common.tick import Tick
from widgets.cb_filter import CBFilter
from widgets.t_open_orders import TOpenOrders
from widgets.t_order_book import TOrderBook
from widgets.cb_orders_pairs import CBOrdersPairs


class MainWindow(QtWidgets.QMainWindow):
    bot_thread = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.init_handlers()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1096, 768)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setMinimumSize(QtCore.QSize(350, 0))
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.comboBox = CBFilter(self.groupBox, pairs=settings.PAIRS)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        self.comboBox.setMinimumSize(QtCore.QSize(0, 25))
        self.comboBox.setObjectName("comboBox")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.comboBox)
        self.verticalLayout_3.addLayout(self.formLayout)
        self.t_open_orders = TOpenOrders(self.groupBox)
        self.t_open_orders.setObjectName("t_open_orders")
        self.t_open_orders.setColumnCount(0)
        self.t_open_orders.setRowCount(0)
        self.verticalLayout_3.addWidget(self.t_open_orders)
        self.horizontalLayout.addWidget(self.groupBox)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setTabPosition(QtWidgets.QTabWidget.North)
        self.tabWidget.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_trade_history = QtWidgets.QWidget()
        self.tab_trade_history.setObjectName("tab_trade_history")
        self.tabWidget.addTab(self.tab_trade_history, "")
        self.tab_open_orders = QtWidgets.QWidget()
        self.tab_open_orders.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tab_open_orders.setObjectName("tab_open_orders")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_open_orders)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        self.cb_orders_pairs = CBOrdersPairs(self.tab_open_orders, pairs=settings.PAIRS)
        self.cb_orders_pairs.setObjectName("cb_orders_pair")
        self.verticalLayout_2.addWidget(self.cb_orders_pairs)

        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.groupBox_2 = QtWidgets.QGroupBox(self.tab_open_orders)
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.t_sell_orders = TOrderBook(self.groupBox_2)
        self.t_sell_orders.setColumnCount(3)
        self.t_sell_orders.setObjectName("t_sell_orders")
        self.t_sell_orders.setRowCount(0)
        self.horizontalLayout_4.addWidget(self.t_sell_orders)
        self.horizontalLayout_3.addWidget(self.groupBox_2)
        self.groupBox_3 = QtWidgets.QGroupBox(self.tab_open_orders)
        self.groupBox_3.setObjectName("groupBox_3")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.groupBox_3)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.t_buy_orders = TOrderBook(self.groupBox_3)
        self.t_buy_orders.setColumnCount(3)
        self.t_buy_orders.setObjectName("t_buy_orders")
        self.t_buy_orders.setRowCount(0)
        self.horizontalLayout_5.addWidget(self.t_buy_orders)
        self.horizontalLayout_3.addWidget(self.groupBox_3)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.tabWidget.addTab(self.tab_open_orders, "")
        self.horizontalLayout.addWidget(self.tabWidget)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.t_rates = QtWidgets.QTableWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.t_rates.sizePolicy().hasHeightForWidth())
        self.t_rates.setSizePolicy(sizePolicy)
        self.t_rates.setMinimumSize(QtCore.QSize(200, 150))
        self.t_rates.setObjectName("t_rates")
        self.t_rates.setColumnCount(0)
        self.t_rates.setRowCount(0)
        self.horizontalLayout_2.addWidget(self.t_rates)
        self.l_rules = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_rules.sizePolicy().hasHeightForWidth())
        self.l_rules.setSizePolicy(sizePolicy)
        self.l_rules.setMinimumSize(QtCore.QSize(500, 150))
        self.l_rules.setObjectName("l_rules")
        self.horizontalLayout_2.addWidget(self.l_rules)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.b_start_btce_bot = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.b_start_btce_bot.sizePolicy().hasHeightForWidth())
        self.b_start_btce_bot.setSizePolicy(sizePolicy)
        self.b_start_btce_bot.setObjectName("b_start_btce_bot")
        self.verticalLayout.addWidget(self.b_start_btce_bot)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(1)

    def init_handlers(self):
        self.b_start_btce_bot.clicked.connect(self.on_b_start_btce_bot_clicked)
        self.comboBox.currentIndexChanged.connect(self.on_comboBox_changed)
        self.cb_orders_pairs.currentIndexChanged.connect(self.on_cb_orders_pairs_changed)

    def on_cb_orders_pairs_changed(self, index):
        self.bot_thread.set_depth_pair(self.cb_orders_pairs.currentData())

    def on_comboBox_changed(self, index):
        self.filter_open_orders(self.comboBox.currentData())


    def filter_open_orders(self, pair_code):
        self.t_open_orders.filter(pair_code)

    def on_b_start_btce_bot_clicked(self):
        if self.bot_thread is None:
            keys_files = "{}/keys.txt".format(settings.PROJECT_ROOT)
            if os.path.isfile(keys_files):
                self.bot_thread = BotThread(keys_files)
                self.bot_thread.get_ticks_signal.connect(self.on_get_ticks)
                self.bot_thread.get_active_orders_signal.connect(self.on_get_active_orders)
                self.bot_thread.get_orders_signal.connect(self.on_get_orders)
                self.bot_thread.set_depth_pair(self.cb_orders_pairs.currentData())
                self.bot_thread.start()
            else:
                mb_error = QtWidgets.QMessageBox(self)
                mb_error.setIcon(QtWidgets.QMessageBox.Critical)
                mb_error.setText("Error. Can't read file with keys:\n{}".format(keys_files))
                mb_error.show()

    def on_get_active_orders(self, active_orders: list):
        self.t_open_orders.refresh(active_orders)

    def on_get_orders(self, orders: dict):
        self.t_sell_orders.refresh(orders['asks'])
        self.t_buy_orders.refresh(orders['bids'])

    def on_get_ticks(self, ticks: dict):
        idx = 0
        self.t_rates.setColumnCount(2)
        self.t_rates.setRowCount(len(ticks))

        for pair, tick in sorted(ticks.items()):
            # print(pair)
            pair_vertical_widget = QtWidgets.QTableWidgetItem(str(idx + 1))
            pair_vertical_widget.setData(QtCore.Qt.UserRole, pair)
            self.t_rates.setVerticalHeaderItem(idx, pair_vertical_widget)

            header_item_pair = QtWidgets.QTableWidgetItem("Pair")
            header_item_btce = QtWidgets.QTableWidgetItem("BTCe")
            self.t_rates.setHorizontalHeaderItem(0, header_item_pair)
            self.t_rates.setHorizontalHeaderItem(1, header_item_btce)

            title = tick.pair.upper().replace('_', '/')
            title_item = QtWidgets.QTableWidgetItem(title)
            title_item.setFlags(title_item.flags() ^ QtCore.Qt.ItemIsEditable)
            last = str(tick.last)
            rate_item = QtWidgets.QTableWidgetItem(last)
            rate_item.setFlags(rate_item.flags() ^ QtCore.Qt.ItemIsEditable)
            self.t_rates.setItem(idx, 0, title_item)
            self.t_rates.setItem(idx, 1, rate_item)

            idx += 1

            if idx >= len(ticks):
                idx = 0

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "BTCe Trader"))
        self.groupBox.setTitle(_translate("MainWindow", "Открытые ордера"))
        self.label.setText(_translate("MainWindow", "Фильтр:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_trade_history),
                                  _translate("MainWindow", "История сделок"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Ордера на продажу"))
        self.groupBox_3.setTitle(_translate("MainWindow", "Ордера на покупку"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_open_orders), _translate("MainWindow", "Стакан"))
        self.b_start_btce_bot.setText(_translate("MainWindow", "Start BTCe Bot"))
