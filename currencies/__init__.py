class Pair(object):
    __name = ''
    __code = ''
    __rate = 0

    def __init__(self, name, code, rate):
        self.__name = name
        self.__code = code
        self.__rate = rate

    @property
    def name(self):
        return self.__name

    @property
    def code(self):
        return self.__code

    @property
    def rate(self):
        return self.__rate


class Currency(object):
    __name = ''
    __code = ''

    def __init__(self, name, code):
        self.__name = name
        self.__code = code

    @property
    def name(self):
        return self.__name

    @property
    def code(self):
        return self.__code
