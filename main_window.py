import os
from PyQt5 import QtCore, QtGui, QtWidgets

from bot.thread import BotThread
import settings
from common.tick import Tick
from widgets.cb_filter import CBFilter
from widgets.t_open_orders import TOpenOrders
from widgets.t_order_book import TOrderBook
from widgets.cb_orders_pairs import CBOrdersPairs
from widgets.t_history_trades import THistoryTrades
from widgets.t_rates import TRates
from widgets.sb_status_bar import SBStatusBar
from rules.rule import Rule


class MainWindow(QtWidgets.QMainWindow):
    bot_thread = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.init_handlers()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1150, 710)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setMinimumSize(QtCore.QSize(370, 0))
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.comboBox = CBFilter(self.groupBox, pairs=settings.PAIRS)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
        self.comboBox.setSizePolicy(sizePolicy)
        self.comboBox.setMinimumSize(QtCore.QSize(0, 25))
        self.comboBox.setObjectName("comboBox")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.comboBox)
        self.verticalLayout_3.addLayout(self.formLayout)
        self.t_open_orders = TOpenOrders(self.groupBox)
        self.t_open_orders.setObjectName("t_open_orders")
        self.t_open_orders.setColumnCount(0)
        self.t_open_orders.setRowCount(0)
        self.verticalLayout_3.addWidget(self.t_open_orders)
        self.horizontalLayout.addWidget(self.groupBox)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setTabPosition(QtWidgets.QTabWidget.North)
        self.tabWidget.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_trade_history = QtWidgets.QWidget()
        self.tab_trade_history.setObjectName("tab_trade_history")
        self.tabWidget.addTab(self.tab_trade_history, "")
        self.tab_open_orders = QtWidgets.QWidget()
        self.tab_open_orders.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tab_open_orders.setObjectName("tab_open_orders")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_open_orders)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        self.cb_orders_pairs = CBOrdersPairs(self.tab_open_orders, pairs=settings.PAIRS)
        self.cb_orders_pairs.setObjectName("cb_orders_pair")
        self.verticalLayout_2.addWidget(self.cb_orders_pairs)

        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.groupBox_2 = QtWidgets.QGroupBox(self.tab_open_orders)
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.t_sell_orders = TOrderBook(self.groupBox_2)
        self.t_sell_orders.setColumnCount(3)
        self.t_sell_orders.setObjectName("t_sell_orders")
        self.t_sell_orders.setRowCount(0)
        self.horizontalLayout_4.addWidget(self.t_sell_orders)
        self.horizontalLayout_3.addWidget(self.groupBox_2)
        self.groupBox_3 = QtWidgets.QGroupBox(self.tab_open_orders)
        self.groupBox_3.setObjectName("groupBox_3")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.groupBox_3)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.t_buy_orders = TOrderBook(self.groupBox_3)
        self.t_buy_orders.setColumnCount(3)
        self.t_buy_orders.setObjectName("t_buy_orders")
        self.t_buy_orders.setRowCount(0)
        self.horizontalLayout_5.addWidget(self.t_buy_orders)
        self.horizontalLayout_3.addWidget(self.groupBox_3)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        self.t_trades = THistoryTrades(self.tab_open_orders)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.t_trades.sizePolicy().hasHeightForWidth())
        self.t_trades.setSizePolicy(sizePolicy)
        self.t_trades.setMinimumSize(QtCore.QSize(0, 150))
        self.t_trades.setMaximumSize(QtCore.QSize(16777215, 150))
        self.t_trades.setColumnCount(5)
        self.t_trades.setObjectName("t_trades")
        self.t_trades.setRowCount(0)
        self.verticalLayout_2.addWidget(self.t_trades)

        self.tabWidget.addTab(self.tab_open_orders, "")
        self.horizontalLayout.addWidget(self.tabWidget)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.t_rates = TRates(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.t_rates.sizePolicy().hasHeightForWidth())
        self.t_rates.setSizePolicy(sizePolicy)
        self.t_rates.setMinimumSize(QtCore.QSize(200, 150))
        self.t_rates.setObjectName("t_rates")
        self.t_rates.setColumnCount(0)
        self.t_rates.setRowCount(0)
        self.horizontalLayout_2.addWidget(self.t_rates)
        self.l_rules = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_rules.sizePolicy().hasHeightForWidth())
        self.l_rules.setSizePolicy(sizePolicy)
        self.l_rules.setMinimumSize(QtCore.QSize(500, 150))
        self.l_rules.setObjectName("l_rules")
        self.horizontalLayout_2.addWidget(self.l_rules)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.b_start_btce_bot = QtWidgets.QPushButton(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.b_start_btce_bot.sizePolicy().hasHeightForWidth())
        self.b_start_btce_bot.setSizePolicy(sizePolicy)
        self.b_start_btce_bot.setObjectName("b_start_btce_bot")
        self.verticalLayout.addWidget(self.b_start_btce_bot)
        MainWindow.setCentralWidget(self.centralwidget)

        self.status_bar = SBStatusBar(self)
        self.setStatusBar(self.status_bar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(1)

    def init_handlers(self):
        self.b_start_btce_bot.clicked.connect(self.on_b_start_btce_bot_clicked)
        self.comboBox.currentIndexChanged.connect(self.on_comboBox_changed)
        self.cb_orders_pairs.currentIndexChanged.connect(self.on_cb_orders_pairs_changed)

    def on_cb_orders_pairs_changed(self, index):
        if self.bot_thread is not None:
            self.bot_thread.set_depth_pair(self.cb_orders_pairs.currentData())

    def on_comboBox_changed(self, index):
        self.filter_open_orders(self.comboBox.currentData())

    def filter_open_orders(self, pair_code):
        self.t_open_orders.filter(pair_code)

    def on_b_start_btce_bot_clicked(self):
        if self.bot_thread is None:
            keys_files = "{}/keys.txt".format(settings.PROJECT_ROOT)
            if os.path.isfile(keys_files):
                self.bot_thread = BotThread(keys_files, settings.MAX_HISTORY_TRADES_CNT)  # type: BotThread
                self.bot_thread.get_ticks_signal.connect(self.on_get_ticks)
                self.bot_thread.get_active_orders_signal.connect(self.on_get_active_orders)
                self.bot_thread.get_orders_signal.connect(self.on_get_orders)
                self.bot_thread.get_trades_signal.connect(self.on_get_trades)
                self.bot_thread.get_update_date_signal.connect(self.on_get_update_date)
                self.bot_thread.set_depth_pair(self.cb_orders_pairs.currentData())
                self.bot_thread.start()
                self.b_start_btce_bot.setEnabled(False)
            else:
                mb_error = QtWidgets.QMessageBox(self)
                mb_error.setIcon(QtWidgets.QMessageBox.Critical)
                mb_error.setText("Error. Can't read file with keys:\n{}".format(keys_files))
                mb_error.show()

    def on_get_update_date(self, date: str):
        self.status_bar.set_last_update(date)

        # rule_str = "BUY BTC_USD 0.123 BY 1150 IF (1 > 2) OR SELL LTC_USD 0.555 BY 12 IF (LTC_USD.PRICE > 555)"
        # rule_str = "SELL LTC_USD 0.1 BY 12.5 IF (2 > 1)"
        rule_str = ""
        if rule_str:
            rules = [rule_str]
        else:
            rules = []
        # rule_str = "BUY BTC_USD 0.123 BY 1150 IF (INFO.WALLET.USD > 554)"
        execute_actions = []
        for rule_str in rules:
            rule = Rule(rule_str)
            execute_action = rule.run()
            if execute_action is not None:
                execute_actions.append(execute_action)

        self.bot_thread.execute_actions = execute_actions

    def on_get_trades(self, trades: list):
        """
        Trades history!
        :param trades:
        :return:
        """
        self.t_trades.refresh(trades)

    def on_get_active_orders(self, active_orders: list):
        self.t_open_orders.refresh(active_orders)

    def on_get_orders(self, orders: dict):
        self.t_sell_orders.refresh(orders['asks'])
        self.t_buy_orders.refresh(orders['bids'])

    def on_get_ticks(self, ticks: dict):
        self.t_rates.refresh(ticks)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "BTCe Trader"))
        self.groupBox.setTitle(_translate("MainWindow", "Открытые ордера"))
        self.label.setText(_translate("MainWindow", "Фильтр:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_trade_history),
                                  _translate("MainWindow", "История сделок"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Ордера на продажу"))
        self.groupBox_3.setTitle(_translate("MainWindow", "Ордера на покупку"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_open_orders), _translate("MainWindow", "Стакан"))
        self.b_start_btce_bot.setText(_translate("MainWindow", "Start BTCe Bot"))
