import os
from collections import OrderedDict
from currencies import Pair, Currency

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

PAIRS = OrderedDict()
PAIRS['btc_usd'] = Pair('BTC/USD', 'btc_usd', 0)
PAIRS['dsh_btc'] = Pair('DSH/BTC', 'dsh_btc', 0)
PAIRS['dsh_usd'] = Pair('DSH/USD', 'dsh_usd', 0)
PAIRS['ltc_usd'] = Pair('LTC/USD', 'ltc_usd', 0)
PAIRS['ltc_btc'] = Pair('LTC/BTC', 'ltc_btc', 0)

CURRENCIES = OrderedDict()
CURRENCIES['usd'] = Currency('USD', 'usd')
CURRENCIES['btc'] = Currency('BTC', 'btc')
CURRENCIES['ltc'] = Currency('LTC', 'ltc')
CURRENCIES['dsh'] = Currency('LTC', 'dsh')

MAX_HISTORY_TRADES_CNT = 100
