import sys
import os

from PyQt5.QtWidgets import QApplication, QWidget

from main_window import MainWindow

from rules.rule import Rule

if __name__ == '__main__':
    app = QApplication(sys.argv)

    w = MainWindow()
    w.show()

    sys.exit(app.exec_())
