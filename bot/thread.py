from datetime import datetime

from PyQt5.QtCore import QThread, pyqtSignal

from bot.btce import Btce
from rules.executor import Executor


class BotThread(QThread):
    bot = None
    __depth_pair = None
    __history_count = 0
    get_ticks_signal = pyqtSignal(dict)
    get_active_orders_signal = pyqtSignal(list)
    get_orders_signal = pyqtSignal(dict)
    get_trades_signal = pyqtSignal(list)  # History
    get_update_date_signal = pyqtSignal(str)  # For update status bar

    execute_actions = []

    def __init__(self, keys_file, history_count=50):
        self.bot = Btce(keys_file)
        self.__history_count = history_count
        super().__init__()

    def set_depth_pair(self, pair):
        self.__depth_pair = pair

    def run(self):
        while 1:
            print(self.execute_actions)

            for action in self.execute_actions:
                executor = Executor(action, self.bot)
                executor.exec()

            ticks = self.bot.getTicks()
            # print('Ticks: ')
            # print(ticks)
            self.get_ticks_signal.emit(ticks)

            active_orders = self.bot.get_active_orders()
            # print(active_orders)
            self.get_active_orders_signal.emit(active_orders)

            orders = self.bot.get_depth(self.__depth_pair)
            self.get_orders_signal.emit(orders)

            history_trades = self.bot.get_trades(self.__depth_pair, self.__history_count)
            self.get_trades_signal.emit(history_trades)

            self.get_update_date_signal.emit(datetime.now().strftime("%H:%M:%S"))

            self.sleep(1)
