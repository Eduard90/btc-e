from collections import OrderedDict
from btceapi.keyhandler import KeyHandler
from btceapi import BTCEConnection, getTickers, TradeAPI, getDepth, getTradeHistory

from bot import Bot
from common.tick import Tick
from common.active_order import ActiveOrder
from common.order import Order
from common.trade import Trade
from settings import PAIRS


class Btce(Bot):
    keys_file = ''
    handler = None

    def __init__(self, keys_file):
        self.keys_file = keys_file
        self.handler = KeyHandler

    def getTicks(self):
        pairs = PAIRS.keys()
        attrs = ('high', 'low', 'avg', 'vol', 'vol_cur', 'last',
                 'buy', 'sell', 'updated')

        connection = BTCEConnection()

        ticks = OrderedDict()
        tickers = getTickers(pairs, connection)

        if isinstance(tickers, dict):
            for pair, ticker in tickers.items():
                low = getattr(ticker, 'low')
                high = getattr(ticker, 'high')
                last = getattr(ticker, 'last')
                updated = getattr(ticker, 'updated')
                tick = Tick(PAIRS[pair], pair, low, high, last, updated)
                ticks[pair] = tick

        return ticks

    def get_active_orders(self):
        connection = BTCEConnection()

        active_orders = []

        with KeyHandler(self.keys_file) as handler:
            for key_ in handler.keys:
                key = key_

            t = TradeAPI(key, handler, connection)
            active_orders_raw = t.activeOrders()
            for ao in active_orders_raw:
                active_order = ActiveOrder(ao.order_id, ao.pair, PAIRS[ao.pair], ao.type, ao.amount, ao.rate,
                                           ao.timestamp_created)
                active_orders.append(active_order)

        return active_orders

    def get_depth(self, pair) -> dict:
        asks, bids = getDepth(pair)

        asks_orders = []
        bids_orders = []

        for ask in asks:
            ask_order = Order(ask[0], ask[1])
            asks_orders.append(ask_order)
        for bid in bids:
            bid_order = Order(bid[0], bid[1])
            bids_orders.append(bid_order)

        return {'asks': asks_orders, 'bids': bids_orders}

    def get_trades(self, pair: str, count: int) -> list:
        history_trades = getTradeHistory(pair, count=count)

        trades = []
        for history_item in history_trades:
            trade = Trade(history_item.pair, history_item.type, history_item.price, history_item.amount,
                          history_item.timestamp)
            trades.append(trade)

        return trades

    def create_order(self, order_type, pair, amount, price):
        connection = BTCEConnection()

        with KeyHandler(self.keys_file) as handler:
            for key_ in handler.keys:
                key = key_

            t = TradeAPI(key, handler, connection)
            print(type(pair))
            print(type(order_type))
            print(type(price))
            print(type(amount))
            result = t.trade(pair, order_type, price, amount)

            print("Trade result:")
            print("   received: {0}".format(result.received))
            print("    remains: {0}".format(result.remains))
            print("   order_id: {0}".format(result.order_id))
            print("      funds:")
            for c, v in result.funds.items():
                print("        {} {}".format(c, v))

            return True


        return False
