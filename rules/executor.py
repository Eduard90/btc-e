from bot.btce import Btce


class Executor(object):
    action = None
    bot = None

    def __init__(self, action, bot: Btce):
        self.action = action
        self.bot = bot

    def exec(self):
        action = self.action.action.action()
        pair = self.action.pair
        amount = float(self.action.how_much)
        price = float(self.action.rate)
        result = self.bot.create_order(action, pair.code, amount, price)
        # print(action)
        # print(pair.code)
        # print(amount)
        # print(price)

        # print(self.action)
