from pyparsing import *

from rules.action import Action
from rules.executor import Executor
from settings import PAIRS, CURRENCIES


class ActionBuy(object):
    @staticmethod
    def action():
        return 'buy'


class ActionSell(object):
    @staticmethod
    def action():
        return 'sell'


class Condition(object):
    parts = []

    def __init__(self, parts):
        self.parts = parts

    def generate_code(self):
        code = []
        for part in self.parts:
            part_value = ""
            if isinstance(part, ConditionPart):
                part_value = str(part.value())
            else:  # Must be str
                part_value = part
            code.append(part_value)

        return ' '.join(code)

    def __str__(self):
        return self.generate_code()


class ConditionPart(object):
    def value(self):
        raise NotImplemented()


class ConditionPartPairField(ConditionPart):
    pair = ''
    field = ''

    def __init__(self, pair, field):
        self.pair = pair
        self.field = field

    def value(self):
        return 666


class ConditionPartInfoWallet(ConditionPart):
    field = ''

    def __init__(self, field):
        self.field = field

    def value(self):
        return 111


class Rule(object):
    actions = []
    rule_str = ""
    parsed = []

    def gen_condition(self, a):
        condition = Condition(a[0])
        return condition

    def gen_part(self, a):
        pair_fields = ('PRICE',)
        condition_part_map = {}
        for pair, value in PAIRS.items():
            for pair_field in pair_fields:
                key = "{}.{}".format(pair.upper(), pair_field)
                condition_part_map[key] = ConditionPartPairField(pair, pair_field)

        for code, currency in CURRENCIES.items():
            key = "INFO.WALLET.{}".format(code.upper())
            condition_part_map[key] = ConditionPartInfoWallet(code.upper())

        return condition_part_map.get(a[0], a[0])

    def __init__(self, rule_str: str):
        actions_map = {'BUY': ActionBuy, 'SELL': ActionSell}

        # lp_map = {k.upper(): LPPairField(k, ) for k, v in PAIRS.items()}

        self.rule_str = rule_str

        WORD = Word(alphanums + '_.')
        OR = 'OR'
        IF = 'IF'
        BY = 'BY'
        GREATER = Literal('>')
        LOWER = Literal('<')
        SIGN = GREATER | LOWER
        LEFT_BRACKET = Suppress('(')
        RIGHT_BRACKET = Suppress(')')
        PAIR = oneOf("BTC_USD LTC_BTC LTC_USD DSH_USD")
        ACTION = Keyword("SELL") | Keyword("BUY")
        HOW_MUCH = Word(alphanums + '.')
        RATE = Word(nums + '.')

        LEFT_PART = WORD.setResultsName("left_part*").setParseAction(lambda s, l, t: self.gen_part(t))
        RIGHT_PART = WORD.setResultsName("right_part*").setParseAction(
            lambda s, l, t: self.gen_part(t))

        CONDITION_PART = (Group(LEFT_PART + SIGN + RIGHT_PART)).setResultsName(
            "conditions*").setParseAction(lambda s, l, t: self.gen_condition(t)) + Optional(OR).setResultsName(
            'operators*')
        CONDITION = IF + LEFT_BRACKET + CONDITION_PART + ZeroOrMore(CONDITION_PART) + RIGHT_BRACKET

        action = (Group(
            ACTION.setParseAction(lambda s, l, t: actions_map[t[0]]).setResultsName('action') + PAIR.setParseAction(
                lambda s, l, t: PAIRS[t[0].lower()]).setResultsName('pair') + HOW_MUCH.setResultsName(
                'how_much') + BY + RATE.setResultsName('rate') + CONDITION)).setResultsName(
            'actions*') + Optional(OR)
        actions = action + ZeroOrMore(action)
        self.parsed = actions.parseString(rule_str)

        for parsed in self.parsed['actions']:
            action = Action(parsed)
            # Need add check action.is_valid()
            self.actions.append(action)

    def run(self):
        action_for_execute = None
        for action in self.actions:
            result = action.check()
            if result:
                action_for_execute = action
                break

        if action_for_execute is not None:
            return action_for_execute

        return None
        #     executor = Executor(action_for_execute)
        #     executor.exec()
