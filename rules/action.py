class Action(object):
    ACTION_BUY = "BUY"
    ACTION_SELL = "SELL"
    ACTIONS = (ACTION_BUY, ACTION_SELL)

    action_list = []
    action = ""
    pair = ""
    how_much = 0
    rate = 0
    conditions = []
    conditions_operators = []
    final_conditions = []
    condition_python_str = ""
    accepted_pair_props = ('PRICE',)

    def __init__(self, action_list):
        self.action_list = action_list
        self.action = self.action_list.action
        self.pair = self.action_list.pair
        self.how_much = self.action_list.how_much
        self.rate = self.action_list.rate
        self.conditions = self.action_list['conditions']
        try:
            self.conditions_operators = self.action_list['operators']
        except KeyError:
            pass

        conditions_tmp = self.conditions.asList()  # type: list

        if len(self.conditions) > 1:
            idx = 1
            for operator in self.conditions_operators:
                conditions_tmp.insert(idx, operator)
                idx += 2

        self.final_conditions = conditions_tmp

    def check(self):
        # print(self.action)
        # print(self.pair)
        # print(self.how_much)
        # print(self.rate)
        # print(self.conditions)
        # print(self.conditions_operators)
        # print(self.final_conditions)

        condition_pseudo_code = ' '.join([str(part) for part in self.final_conditions])

        condition_py = condition_pseudo_code.lower()

        result = eval(condition_py)  # type: bool
        return result

    def is_valid(self):
        if self.action not in Action.ACTIONS:
            return False

        # TODO: Add check min_amount and min_price!

        return True
