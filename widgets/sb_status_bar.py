from PyQt5 import QtWidgets


class SBStatusBar(QtWidgets.QStatusBar):
    __message_format = "Last update: {last_update}"

    def set_last_update(self, date: str):
        self.showMessage(self.__message_format.format(last_update=date))
