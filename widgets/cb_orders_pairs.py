from PyQt5 import QtWidgets

if False:
    from currencies import Pair


class CBOrdersPairs(QtWidgets.QComboBox):
    pairs = None

    def __init__(self, *args, **kwargs):
        self.pairs = kwargs.pop('pairs', None)
        super().__init__(*args, **kwargs)

        if self.pairs:
            for pair, value in self.pairs.items():  # type: str, Currency
                self.addItem(value.name, value.code)
