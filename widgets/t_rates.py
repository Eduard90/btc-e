from PyQt5 import QtWidgets, QtCore

from widgets.read_widget_item import ReadWidgetItem


class TRates(QtWidgets.QTableWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.setStyleSheet("font-size: 11px;")

    def refresh(self, ticks: dict):
        idx = 0
        self.setColumnCount(2)
        self.setRowCount(len(ticks))

        for pair, tick in sorted(ticks.items()):
            color = QtCore.Qt.black

            prev_btce_item = self.item(idx, 1)
            if prev_btce_item is not None:
                prev_btce_val = float(prev_btce_item.text())
                last = float(tick.last)
                if last > prev_btce_val:
                    color = QtCore.Qt.darkGreen
                elif last < prev_btce_val:
                    color = QtCore.Qt.red
                elif last == prev_btce_val:
                    color = QtCore.Qt.black

            pair_vertical_widget = QtWidgets.QTableWidgetItem(str(idx + 1))
            pair_vertical_widget.setData(QtCore.Qt.UserRole, pair)
            self.setVerticalHeaderItem(idx, pair_vertical_widget)

            header_item_pair = QtWidgets.QTableWidgetItem("Pair")
            header_item_btce = QtWidgets.QTableWidgetItem("BTCe")
            self.setHorizontalHeaderItem(0, header_item_pair)
            self.setHorizontalHeaderItem(1, header_item_btce)

            title = tick.pair.upper().replace('_', '/')
            title_item = ReadWidgetItem(title)
            last = str(tick.last)
            rate_item = ReadWidgetItem(last)
            rate_item.setForeground(color)

            self.setItem(idx, 0, title_item)
            self.setItem(idx, 1, rate_item)

            idx += 1
