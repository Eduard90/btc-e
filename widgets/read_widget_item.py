from PyQt5 import QtWidgets, QtCore


class ReadWidgetItem(QtWidgets.QTableWidgetItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setFlags(self.flags() ^ QtCore.Qt.ItemIsEditable)
