from PyQt5 import QtWidgets, QtCore
from widgets.read_widget_item import ReadWidgetItem


class THistoryTrades(QtWidgets.QTableWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setStyleSheet("font-size: 11px;")

    def refresh(self, trades: list):
        idx = 0
        self.setRowCount(len(trades))

        header_item_type = QtWidgets.QTableWidgetItem("Type")
        header_item_amount = QtWidgets.QTableWidgetItem("Amount")
        header_item_rate = QtWidgets.QTableWidgetItem("Rate")
        header_item_price = QtWidgets.QTableWidgetItem("Price")
        header_item_date = QtWidgets.QTableWidgetItem("Date")
        self.setHorizontalHeaderItem(0, header_item_type)
        self.setHorizontalHeaderItem(1, header_item_rate)
        self.setHorizontalHeaderItem(2, header_item_amount)
        self.setHorizontalHeaderItem(3, header_item_price)
        self.setHorizontalHeaderItem(4, header_item_date)

        self.setColumnWidth(0, 30)
        self.setColumnWidth(1, 85)
        self.setColumnWidth(2, 75)
        self.setColumnWidth(3, 115)
        self.setColumnWidth(4, 130)

        for trade in trades:
            type_item = QtWidgets.QTableWidgetItem(trade.type)
            type_item.setFlags(type_item.flags() ^ QtCore.Qt.ItemIsEditable)
            if trade.type == 'ask':
                type_item.setForeground(QtCore.Qt.red)
            else:
                type_item.setForeground(QtCore.Qt.darkGreen)

            rate_item = ReadWidgetItem(str(trade.rate))
            amount_item = ReadWidgetItem(str(trade.amount))
            price_item = ReadWidgetItem(str(trade.price))
            date_item = ReadWidgetItem(trade.date)

            self.setItem(idx, 0, type_item)
            self.setItem(idx, 1, rate_item)
            self.setItem(idx, 2, amount_item)
            self.setItem(idx, 3, price_item)
            self.setItem(idx, 4, date_item)

            idx += 1

        self.resizeRowsToContents()
