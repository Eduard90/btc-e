from PyQt5 import QtWidgets, QtCore

from widgets.read_widget_item import ReadWidgetItem


class TOpenOrders(QtWidgets.QTableWidget):
    __filter_pair = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def filter(self, pair):
        self.__filter_pair = pair

        if self.__filter_pair is not None:
            for row in range(self.rowCount()):
                vert_item = self.verticalHeaderItem(row)
                data = vert_item.data(QtCore.Qt.UserRole)
                if data != self.__filter_pair:
                    self.hideRow(row)
                else:
                    self.showRow(row)
        else:
            for row in range(self.rowCount()):
                self.showRow(row)

    def refresh(self, orders):
        idx = 0
        self.setColumnCount(4)
        self.setRowCount(len(orders))

        header_item_type = QtWidgets.QTableWidgetItem("Type")
        header_item_amount = QtWidgets.QTableWidgetItem("Amount")
        header_item_rate = QtWidgets.QTableWidgetItem("Rate")
        header_item_date = QtWidgets.QTableWidgetItem("Date")
        self.setHorizontalHeaderItem(0, header_item_type)
        self.setHorizontalHeaderItem(1, header_item_amount)
        self.setHorizontalHeaderItem(2, header_item_rate)
        self.setHorizontalHeaderItem(3, header_item_date)

        self.setStyleSheet("font-size: 11px;")

        self.setColumnWidth(0, 30)
        self.setColumnWidth(1, 70)
        self.setColumnWidth(2, 73)
        self.setColumnWidth(3, 115)

        for order in orders:
            pair_vertical_widget = QtWidgets.QTableWidgetItem(order.currency.name)
            pair_vertical_widget.setData(QtCore.Qt.UserRole, order.currency.code)
            self.setVerticalHeaderItem(idx, pair_vertical_widget)

            type_item = ReadWidgetItem(order.type)
            amount_item = ReadWidgetItem(str(order.amount))
            rate_item = ReadWidgetItem(str(order.rate))
            date_item = ReadWidgetItem(str(order.created_at))

            self.setItem(idx, 0, type_item)
            self.setItem(idx, 1, amount_item)
            self.setItem(idx, 2, rate_item)
            self.setItem(idx, 3, date_item)

            # self.resizeColumnsToContents()
            self.resizeRowsToContents()

            idx += 1

        self.filter(self.__filter_pair)
