from PyQt5 import QtWidgets, QtCore, QtGui

from widgets.read_widget_item import ReadWidgetItem

if False:
    from common.order import Order


class TOrderBook(QtWidgets.QTableWidget):
    __big_order_amount_font = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__set_style()

    def __set_style(self):
        self.setStyleSheet("font-size: 11px;")

        self.__big_order_amount_font = QtGui.QFont()
        self.__big_order_amount_font.setBold(True)

    def __highlight_big_orders(self, orders_rows: dict):
        amounts = list(orders_rows.keys())
        max_cnt = 5
        if len(amounts) < max_cnt:
            max_cnt = len(amounts)

        for _ in range(max_cnt):
            max_ = max(amounts)
            row_idx = orders_rows[max_]
            for col in range(self.columnCount()):
                item = self.item(row_idx, col)  # type: QtWidgets.QTableWidgetItem
                if item is not None:
                    item.setFont(self.__big_order_amount_font)
                    item.setForeground(QtCore.Qt.darkRed)

            del amounts[amounts.index(max_)]

    def refresh(self, orders: list):
        idx = 0
        self.setColumnCount(3)
        self.setRowCount(len(orders))

        header_item_rate = QtWidgets.QTableWidgetItem("Rate")
        header_item_amount = QtWidgets.QTableWidgetItem("Amount")
        header_item_price = QtWidgets.QTableWidgetItem("Price")
        self.setHorizontalHeaderItem(0, header_item_rate)
        self.setHorizontalHeaderItem(1, header_item_amount)
        self.setHorizontalHeaderItem(2, header_item_price)

        self.setColumnWidth(0, 65)
        self.setColumnWidth(2, 120)

        orders_rows = dict()

        for order in orders:  # type: Order
            rate_item = ReadWidgetItem(str(order.rate))
            amount_item = ReadWidgetItem(str(order.amount))
            price_item = ReadWidgetItem(str(order.price))

            self.setItem(idx, 0, rate_item)
            self.setItem(idx, 1, amount_item)
            self.setItem(idx, 2, price_item)

            orders_rows[order.amount] = idx

            idx += 1

        self.__highlight_big_orders(orders_rows)

        self.resizeRowsToContents()
